plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.21"
    id("org.jetbrains.kotlin.kapt") version "1.5.21"
    id("io.micronaut.library") version "2.0.4"
    id("com.github.johnrengelman.shadow") version "7.0.0"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.5.21"
}

version = "0.1"
group = "fabric.search"

val kotlinVersion= project.properties["kotlinVersion"]
val kotestVersion= project.properties["kotestVersion"]
val mockkVersion= project.properties["mockkVersion"]

repositories {
    mavenCentral()
}

micronaut {
    runtime("lambda")
    testRuntime("kotest")
    processing {
        incremental(true)
        annotations("fabric.search.*")
    }
}

dependencies {
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    implementation("io.micronaut.aws:micronaut-function-aws:3.0.0")
    implementation("io.micronaut.kotlin:micronaut-kotlin-runtime:3.0.0")
    implementation("io.micronaut:micronaut-runtime:3.0.1")
    implementation("io.micronaut:micronaut-validation:3.0.1")
    implementation("org.jetbrains.kotlin:kotlin-reflect:${kotlinVersion}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlinVersion}")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.5")
    implementation("software.amazon.awssdk:sns:2.17.40")
    implementation("software.amazon.awssdk:sqs:2.17.40")
    implementation("io.micronaut.aws:micronaut-aws-sdk-v2:3.0.0")

    implementation("io.micronaut:micronaut-http-client:3.0.1")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
//    implementation("com.squareup.retrofit2:converter-jackson")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")

    api("aws.sdk.kotlin:sns:0.4.0-alpha")
    implementation("com.amazonaws:aws-lambda-java-events:3.10.0")

//    testImplementation("io.micronaut:micronaut-http-client:3.0.1")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.6")

    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotestVersion") // optional, for kotest assertions
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotestVersion") // required
    testImplementation("io.kotest:kotest-property:$kotestVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")

}



java {
    sourceCompatibility = JavaVersion.toVersion("11")
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "11"
        }
    }
    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "11"
        }
    }

}
tasks.named("assemble") {
    dependsOn(":shadowJar")
}
tasks.withType<Test> {
    useJUnitPlatform()
}

