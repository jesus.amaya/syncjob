package fabric.search.configuration

import io.micronaut.context.annotation.EachProperty
import io.micronaut.context.annotation.Parameter
import io.micronaut.core.annotation.Introspected


@Introspected
@EachProperty("dependency")
class DependencyConfiguration
constructor(@param:Parameter val name: String){

  var account: String = ""
  var stage: String = ""
  var platform: String = ""
  var serviceName: String = ""
  var apiUrl: String = ""
  var apiKey: String = ""
  var channel: Int = 12

}
