package fabric.search.syncjob

import com.amazonaws.services.lambda.runtime.events.SNSEvent
import io.micronaut.core.annotation.Introspected


class CustomersConfiguration : ArrayList<CustomerConfigurationItem>()

data class CustomerConfigurationItem(
    val customerId: String, // 60dcc089167f8e0008ab15f6
    val syncRules: SyncRules,
    val featureFlags: FeatureFlags,
    val facetsRules: List<FacetsRule>
)

data class SyncRules(
    val fullSyncProcess: FullSyncProcess
)

data class FeatureFlags(
    val allowZeroPrice: Boolean, // false
    val shouldSaveVariants: Boolean // false
)

data class FacetsRule(
    val attributeName: String, // Is Premium
    val type: List<String>
)

data class FullSyncProcess(
    val enable: Boolean, // true   //check if true then check interval and check with current time
    val interval: String // 15 * * * *
)

@Introspected
class CustomerAccountStage {
    var customerId: String? = null
    var stage: String = "sandbox"
    override fun toString(): String {
        return """ {"customerId": "$customerId", "stage": "$stage" } """
    }
}

data class Sns(
    var MessageAttributes: Map<String?, SNSEvent.MessageAttribute?>? = null,
    val SigningCertURL: String? = null,
    val MessageId: String? = null,
    val Message: String? = null,
    val Subject: String? = null,
    val UnsubscribeURL: String? = null,
    val Type: String? = null,
    val SignatureVersion: String? = null,
    val Signature: String? = null,
    val Timestamp: String? = null,
    val TopicArn: String? = null
)
