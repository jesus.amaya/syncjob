package fabric.search.syncjob

import aws.sdk.kotlin.runtime.UnknownServiceErrorException
import aws.sdk.kotlin.services.sns.SnsClient
import aws.sdk.kotlin.services.sns.model.SnsException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.micronaut.core.annotation.Introspected
import jakarta.inject.Singleton
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory


/**
 * Customer configuration service
 *
 * For now its bein
 *
 * @constructor Create empty Customer configuration service
 */
@Singleton
@Introspected
class CustomerConfigurationService {

  private val logger: Logger = LoggerFactory.getLogger(CustomerConfigurationItem::class.java)
  private val mapper = jacksonObjectMapper()
  private val arnTopic = "arn:aws:sns:us-east-1:781540072851:syncjob_customer_account_ids"
  private val snsClient = SnsClient { region = "us-east-1" }


  /**
   * Get customers accounts from a "Service" that are eligible for syncing
   *
   * For the moment there is no such service and we are mocking the service from
   * customersConfig.json file via getCustomersConfig method
   * and so this method is not being used for the current worflow
   *
   * @return a list of customers to sync with their own configuration like Feature flags
   */
  fun getCustomersToSync(): List<CustomerConfigurationItem> = runBlocking {
    logger.info("Starting Customer Configuration")

    val customersToSync = mutableListOf<CustomerConfigurationItem>()

    val customerConfigurationListFromJson: List<CustomerConfigurationItem> = mapper.readValue(getCustomersConfig())

    customerConfigurationListFromJson.forEach { customerConfigurationItem ->
      if (customerConfigurationItem.syncRules.fullSyncProcess.enable) {
        customersToSync.add(customerConfigurationItem)
      }
    }
    launch {
      publishToTopic(snsClient, arnTopic, getCustomersConfig())
      logger.info("Finishing Customer Configuration")
    }
    return@runBlocking customersToSync
  }

  /**
   * Send customers to sns
   *
   * @param customersToSync
   */
  private fun sendCustomersToSns(customersToSync: List<CustomerAccountStage>) = runBlocking {

    logger.info("Starting sendCustomersToSns", customersToSync.toString())

    for (customer in customersToSync) {
      publishToTopic(snsClient, arnTopic, customer.toString())
      logger.info("publishing ${customer.toString()}")
    }
    logger.info("Finishing sendCustomersToSns")
  }


  private suspend fun publishToTopic(
    snsClient: SnsClient,
    arnOfTopic: String,
    messageToSend: String
  ): String {
    return try {
      val publishTopicResponse = snsClient.publish {
        topicArn = arnOfTopic
        message = messageToSend
      }
      logger.info("TopicResponse ID: " + publishTopicResponse.messageId)
      ("Successfully published message to topic with ARN: $arnOfTopic")
    } catch (e: SnsException) {
      ("ERROR (SnsException): " + e.message)
    } catch (e: UnknownServiceErrorException) {
      ("ERROR (UnknownServiceErrorException): " + e.message)
    }
  }


  fun getAccountsIds(): MutableList<CustomerAccountStage> {

    val accountsToSync: MutableList<CustomerAccountStage> = arrayListOf()
    val sandboxCustomerAccounts ="61490ba077f90600095880a5,612d24a655625a0009676700, 61292cbbbed17200097d42e1, 61264a399fcae20009e2717a, 6113dad6138ed50008e1c395, 6102f6141910d20008f764de,60f9d3d2616c7b0008862c6a, 60f59f9f4462b600088fe877, 60f830a609e23e0008a358d7, 60ec78e9fb4cf000085cf0b9, 60d8cfdbe9ff6000083195d7,60c91750fa8d3e000854e8ea, 5f68eb5b5d00e10008872cb9, 5f72dc0a1f33f4000711fd74, 5f7586753840b90009600e2d, 5fb4fc36b03ba40007d32370,5fc78dfc6135050007191290, 5fcf62dbfe3c940007056e42, 5fd776d015f1ef00073d6b56, 6049545e296c40000710d1c7, 6061f5708d711f0007c01438,604fd6dde0a3f40007fd7b43, 60708c003dac0300070be6d9, local, 60aa78423100fd0008e9e54e, 60b0f430adf0c00009e49aa3,60b63d9e7ce90800085548cf, 60ca00ce3b84f40008c1c956, 60ad7e9d858eb50007abbb19, 60c2510300d1d10008cef9c9, 60e4704ff8eae10008b1cec6"
      .split(",")
//    val customerAccounts = "61490ba077f90600095880a5,612d24a655625a0009676700, 61292cbbbed17200097d42e1".split(",")

    sandboxCustomerAccounts.forEach { sandboxAccountId ->
      val newCustomerAccount = CustomerAccountStage()
      newCustomerAccount.customerId = sandboxAccountId
//      logger.info("CustomerAccount $newCustomerAccount Added to the SNS List... ")
      accountsToSync.add(newCustomerAccount)
    }

    sendCustomersToSns(accountsToSync)
    return accountsToSync
  }


  private fun getCustomersConfig(): String {

    return """[
              {
                "customerId": "60d8cfdbe9ff6000083195d7",
                "syncRules": {
                  "fullSyncProcess": {
                    "enable": true,
                    "interval": "15 * * * *"
                  }
                },
                "featureFlags": {
                  "allowZeroPrice": true,
                  "shouldSaveVariants": true
                },
                "facetsRules": [
                  {
                    "attributeName": "Is Premium",
                    "type": [
                      "searchable",
                      "filterOnly"
                    ]
                  }
                ]
              },
              {
                "customerId": "5f72dc0a1f33f4000711fd74",
                "syncRules": {
                  "fullSyncProcess": {
                    "enable": true,
                    "interval": "15 * * * *"
                  }
                },
                "featureFlags": {
                  "allowZeroPrice": false,
                  "shouldSaveVariants": true
                },
                "facetsRules": [
                  {
                    "attributeName": "Is Premium",
                    "type": [
                      "searchable",
                      "filterOnly"
                    ]
                  }
                ]
              },
              {
                "customerId": "60dcc089167f8e0008ab15f6",
                "syncRules": {
                  "fullSyncProcess": {
                    "enable": false,
                    "interval": "15 * * * *"
                  }
                },
                "featureFlags": {
                  "allowZeroPrice": false,
                  "shouldSaveVariants": false
                },
                "facetsRules": [
                  {
                    "attributeName": "Is Premium",
                    "type": [
                      "searchable",
                      "filterOnly"
                    ]
                  }
                ]
              }
            ]""".trimIndent()
  }
}