package fabric.search.handler

import fabric.search.Configuration
import fabric.search.syncjob.CustomerAccountStage
import fabric.search.syncjob.CustomerConfigurationService
import io.micronaut.core.annotation.Introspected
import io.micronaut.function.aws.MicronautRequestHandler
import jakarta.inject.Inject


@Introspected
class CustomersConfigurationRequestHandler :
  MicronautRequestHandler<Configuration?, MutableList<CustomerAccountStage>?>() {
  @Inject
  private lateinit var customerConfigurationService: CustomerConfigurationService

  override fun execute(input: Configuration?): MutableList<CustomerAccountStage>? {
    return if (input != null) {
      return customerConfigurationService.getAccountsIds()
    } else {
      null
    }
  }
}