package fabric.search.handler


import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import fabric.search.product.PimItemIds
import fabric.search.product.PimItemIdsService
import fabric.search.product.ProductService
import fabric.search.syncjob.CustomerAccountStage
import fabric.search.product.Product
import fabric.search.syncjob.Sns
import io.micronaut.core.annotation.Introspected
import io.micronaut.function.aws.MicronautRequestHandler
import jakarta.inject.Inject
import org.slf4j.Logger
import org.slf4j.LoggerFactory


@Introspected
class PimDataRequestHandler : MicronautRequestHandler<SQSEvent?, List<Product?>?>() {

  private val logger: Logger = LoggerFactory.getLogger(PimDataRequestHandler::class.java)

  private val mapper = jacksonObjectMapper()

  @Inject
  private lateinit var productService: ProductService
//  private lateinit var pimItemIdsService: PimItemIdsService //For PIM 1 & 2

  override fun execute(input: SQSEvent?): List<Product?>? {
    return if (input != null) {
      val sns: Sns = mapper.readValue(input.records[0].body!!)
      val customerAccountStage: CustomerAccountStage = mapper.readValue(sns.Message!!)
      return productService.getItems(size = 1, page = 1, accountStage = customerAccountStage)

    //      return pimItemIdsService.getItemIds("ACTIVE",1,5,customerAccountStage) //Uncomment for PIM 1 & 2
    } else {
      null
    }
  }

}
