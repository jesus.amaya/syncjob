package fabric.search.product
import com.fasterxml.jackson.annotation.JsonProperty


data class PimItemIds(
    val itemIds: List<Int>,
    val count: Int, // 13430
    val totalPages: Int // 2686
)