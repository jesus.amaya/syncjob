package fabric.search.product

import com.fasterxml.jackson.annotation.JsonProperty


data class PimResponse(
    val pageSize: Int, // 1
    val totalSize: Int, // 93
    val pages: Int, // 93
    val products: List<Product>
)

data class Product(
    @JsonProperty("_id")
    val id: String?, // 61438874b0b032da2a2ce08e
    val sku: String, // PRODBBCOM012
    val itemId: Int, // 1337
    val children: List<Children>,
    val type: String, // ITEM
    val status: Boolean, // true
    val bundleItems: List<Any>,
    val categories: List<Category?>?,
    val attributes: List<AttributeX?>?,
    val variants: List<Any>,
    val createdOn: String, // 2021-09-16T18:09:56.315Z
    val modifiedOn: String // 2021-09-16T18:13:40.029Z
)

data class Children(
    @JsonProperty("_id")
    val id: String, // 61438876b0b032da2a2ce0e6
    val sku: String, // BBCOM6360217
    val status: Boolean, // false
    val attributes: List<Attribute>,
    val variants: List<Any>,
    val itemId: Int, // 1340
    val createdOn: String, // 2021-09-16T18:09:58.731Z
    val modifiedOn: String // 2021-09-16T18:09:58.731Z
)

data class Category(
    val id: String?, // 61437cd9023c93735570a2e3
    val name: String, // Clothing Product
    val breadcrumbs: List<Breadcrumb>
)

data class AttributeX(
    val name: String, // Product Title
    val description: String, // description
    val mapping: String?, // title
    val type: String, // TEXT
    val value: String // Women's BBcom Skull Cropped Tee
)

data class Attribute(
    val name: String, // Product Title
    val description: String, // description
    val mapping: String?, // title
    val type: String, // TEXT
    val value: String // Women's BBcom Skull Cropped Tee
)

data class Breadcrumb(
    val id: String, // 60c9ef1257c9b00008cc4231
    val name: String, // MASTER
    val attributes: List<Any>
)