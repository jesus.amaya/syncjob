package fabric.search.product

import fabric.search.syncjob.CustomerAccountStage
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.uri.UriBuilder
import jakarta.inject.Singleton
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URI

@Singleton
class ProductService(
  @param:Client("\${dependency.product.apiUrl}") private val httpClient: HttpClient
) {

  private val logger: Logger = LoggerFactory.getLogger(ProductService::class.java)

  fun getItems(
    size: Int? = null,
    page: Int? = null,
    status: String? = null,
    accountStage : CustomerAccountStage
  ): List<Product> {


    val uri: URI = UriBuilder.of("/product")
      .queryParam("page", page)
      .queryParam("size", size)
      .queryParam("status", status)
      .build()
    val date = "2021-08-09T15:17:33.500Z"
    val channel = 12
    val header =
      "{\"date\":\"$date\",\"channel\":$channel,\"account\":\"${accountStage.customerId}\", \"stage\":\"${accountStage.stage}\" }"

//    logger.info("Starting Request with URI =  ${uri}")
    logger.info("Starting Request with HEADER =  ${header}")

    val req: HttpRequest<*> = HttpRequest.GET<Any>(uri)
      .header(HttpHeaders.CONTENT_TYPE, "application/json")
      .header("X-Api-Key", "d41d8cd98f00b204e9800998ecf8427e")
      .header("x-site-context", header)
    logger.info("REQUEST for Account: ${accountStage.customerId}: ${req} ")

    val result = httpClient.toBlocking().retrieve(req, PimResponse::class.java)
    logger.info("RESULT : $result ")

    return result.products
  }


}
