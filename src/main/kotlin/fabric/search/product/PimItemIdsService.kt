package fabric.search.product

import fabric.search.syncjob.CustomerAccountStage
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.uri.UriBuilder
import jakarta.inject.Singleton
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URI

@Singleton
class PimItemIdsService(
  @param:Client("\${dependency.pimApi.idemids.apiUrl}")
  private val httpClient: HttpClient
) {

  private val logger: Logger = LoggerFactory.getLogger(PimItemIdsService::class.java)

  fun getItemIds(
    status: String? = null,
    page: Int? = null,
    limit: Int? = null, // this is the same as size
    customerAccountStage : CustomerAccountStage
  ): List<Int> {

    val uri: URI = UriBuilder.of("/itemIds")
      .queryParam("status", status)
      .queryParam("page", page)
      .queryParam("limit", limit)
      .build()
    val date = "2021-08-25T13:29:21.435Z"
    val channel = 12
    val header ="""
      {"stage":"${customerAccountStage.stage}","account":"${customerAccountStage.customerId}","date":"$date","channel":$channel}""".trimIndent()

//    logger.info("Starting Request with URI =  ${uri}")
    logger.info("Starting Request with HEADER =  $header")

    val req: HttpRequest<*> = HttpRequest.GET<Any>(uri)
      .header(HttpHeaders.CONTENT_TYPE, "application/json")
      .header("X-Api-Key", "d41d8cd98f00b204e9800998ecf8427e")
      .header("x-site-context", header)
    logger.info("REQUEST for Account: ${customerAccountStage.customerId}: $req ")

    val result = httpClient.toBlocking().retrieve(req, PimItemIds::class.java)
    logger.info("RESULT : ${result} ")

    return result.itemIds
  }


}
