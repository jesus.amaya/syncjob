package fabric.search
import io.micronaut.core.annotation.Introspected

@Introspected
class Configuration {
    var name: String? = null
}