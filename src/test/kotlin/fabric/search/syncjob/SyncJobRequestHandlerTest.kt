package fabric.search.syncjob

import fabric.search.handler.CustomersConfigurationRequestHandler
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.core.spec.style.StringSpec
import fabric.search.Configuration
import io.kotest.matchers.types.shouldBeSameInstanceAs
import jakarta.inject.Inject

class SyncJobRequestHandlerTest : StringSpec({


  "SyncJobRequest handler" {
    val customersConfigurationRequestHandler = CustomersConfigurationRequestHandler()

    val configurationObject = Configuration()
    configurationObject.name= "start"
    val result = customersConfigurationRequestHandler.execute(configurationObject)
    result.shouldNotBeNull()
    customersConfigurationRequestHandler.applicationContext.close()
  }
})
//MutableList<CustomerAccountStage>?



